import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Parser {
	
	private static String input;
	private static int index;
	private static BufferedReader reader;
	
	public static void main(String[] args) throws IOException {
		reader = new BufferedReader(new InputStreamReader(System.in));

		input = reader.readLine();
		index = 0;
		
		S();
		
		if (index != input.length()) {
			System.out.format("%nNE");
		} else {
			System.out.format("%nDA");
		}
		
		
		reader.close();
	}

	private static void S() throws IOException {
		System.out.print("S");
		
		char inCh = getCurrentCharacter();
		
		if (inCh != 'a' && inCh != 'b') {
			printNotInLanguage();
		}
		
		index++;
		
		if (inCh == 'a') {
			A();
			B();
		} else {
			B();
			A();
		}
	}

	
	private static void A() throws IOException {
		System.out.print("A");
		
		char inCh = getCurrentCharacter();
		
		switch (inCh) {
			case 'b':
				index++;
				C();
				break;
			case 'a':
				index++;
				break;
			default:
				printNotInLanguage();
		}
		
	}
	
	


	private static void B() throws IOException {
		System.out.print("B");
		
		char inCh = getCurrentCharacter();
		
		if (inCh == 'c') {
			index++;
			inCh = getCurrentCharacter();
			
			if (inCh != 'c') {
				printNotInLanguage();
			}
			
			index++;
			
			S();
			
			inCh = getCurrentCharacter();
			
			if (inCh != 'b') {
				printNotInLanguage();
			}
			
			index++;
			inCh = getCurrentCharacter();
			
			if (inCh != 'c') {
				printNotInLanguage();
			}
			
			index++;
		}
	}
	
	
	private static void C() throws IOException {
		System.out.print("C");
		
		A();
		A();
	}

	
	
	

	private static void printNotInLanguage() throws IOException {
		System.out.format("%nNE%n");
		reader.close();
		System.exit(0);
	}
	
	private static char getCurrentCharacter() {
		if (index < input.length()) {
			return input.charAt(index);
		}
		return ' ';
	}
	
}
